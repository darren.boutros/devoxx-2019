package fr.devoxx.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MissionRestController {
	
	@Autowired
	private MissionRepository missionRepository; 

//	@PostMapping(value = "/mission")
//	public ResponseEntity<ObjectId> createMission(@RequestBody Mission mission){
//		
//		ObjectId id = new ObjectId(missionRepository.save(mission).getId());
//		
//		System.err.print(id);
//		return ResponseEntity
//				.status(HttpStatus.CREATED)
//				.header("Content-type", MediaType.APPLICATION_JSON_VALUE)
//				.body(id);
//	}
	
	@PostMapping(value = "/mission")
	public ResponseEntity<Mission> createMission(@RequestBody Mission mission){

		return ResponseEntity
				.status(HttpStatus.CREATED)
				.header("Content-type", MediaType.APPLICATION_JSON_VALUE)
				.body(missionRepository.save(mission));
	}
}
