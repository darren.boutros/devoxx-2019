package fr.devoxx.spring;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

public interface MissionRepository extends CrudRepository<Mission, UUID> {

}
