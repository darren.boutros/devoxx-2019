package fr.devoxx.spring;


import java.util.UUID;

import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import io.restassured.module.mockmvc.RestAssuredMockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MissionApplication.class)
public abstract class MissionTest {

	@Autowired MissionRestController missionRestController;
	
	@MockBean MissionRepository missionRepository;
	
	@Before
	public void setup() {
		RestAssuredMockMvc.standaloneSetup(missionRestController);
		
		Mission mission = new Mission();
		mission.setId(UUID.fromString("3c2a95ad-4567-4e30-b7fc-c3a9203e78d5"));
		mission.setName("toto");

		when(missionRepository.save(any(Mission.class))).thenReturn(mission);
	}
}
